export interface ShoppingEntity {
  primaryId: number;
  id: number;
  name: string;
  quantity: number;
}
