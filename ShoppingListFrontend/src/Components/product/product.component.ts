import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IProduct } from '../../Shared/Models/IProduct';
import { ProductService } from '../../Services/product.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product: IProduct | undefined;
  productForm : FormGroup;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      const id = +param;
      this.getProduct(id);
    }
  }

  getProduct(id : number) {
    this.productService.getProduct(id).subscribe((product) => this.product = product,
    (error) => console.log(error),
    () => this.createForm());
  }

  createForm() {
    if (!this.product) {
      this.router.navigate(['/product/add']);
      return;
    }

    this.productForm = this.formBuilder.group({
      id: [ this.product.id, [], []],
      name: [ this.product.name, [], []],
      mass: [ this.product.mass, [], []]
    });
  }

  onSubmit() {
    const result: IProduct = Object.assign({}, this.productForm.getRawValue()); // shallow copy
    console.log(result);
  }

}
