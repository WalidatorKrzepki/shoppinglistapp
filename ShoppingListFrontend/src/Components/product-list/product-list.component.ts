import { Component, OnInit } from '@angular/core';
import { IProduct } from '../../Shared/Models/IProduct';
import { ProductService } from '../../Services/product.service';
import { Router } from '@angular/router';
//import { MatTableModule } from '@angular/material/table';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {

  productList: IProduct[] = [];
  displayedColumns: string[] = ['id2', 'name2', 'mass2', 'edit'];
  dataSource : any;

  constructor(private productService : ProductService,
    private router: Router) {
  }

  ngOnInit() {
      this.productService.getProductList().subscribe((productList) => {this.productList = productList; this.dataSource = productList});
      //this.productService.getProductList().subscribe((productList) => this.dataSource = productList);
  }

  editProduct(id : number) {
    console.log("srololo");
    this.router.navigate(['/product/' + id]);
  }
}
