import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from '../Components/product-list/product-list.component';
import { ShoppingListComponent } from '../Components/shopping-list/shopping-list.component';
import { ProductComponent } from '../Components/product/product.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {path: 'shoppingList', component: ShoppingListComponent},
    {path: 'productList', component: ProductListComponent},
    {path: 'product/:id', component: ProductComponent},
    {path: '', component: ShoppingListComponent},
    {path: '**', component: ShoppingListComponent}
  ])
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
